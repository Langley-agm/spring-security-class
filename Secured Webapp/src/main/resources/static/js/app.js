
    var app = angular.module("MyApp", []);

        app.controller("ContactsController", function($scope, $http) {

            $scope.getContacts = function(){
                $http.get('rest/contacts').
                    success(function(data) {
                        $scope.contacts = data;
                    }).
                    error(function(data) {
                        console.log(data);
                    });
            }

            $scope.delete = function(contact){
                $http.delete('rest/contact', { params: { id: contact.id } }).
                    success(function(data) {
                        console.log('Successfully deleted contact.');
                        $scope.getContacts();
                    }).
                    error(function(data) {
                        console.log(data);
                    });
            }

            $scope.getContacts();

        });