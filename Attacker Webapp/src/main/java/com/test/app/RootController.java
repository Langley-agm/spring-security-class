package com.test.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by armando garcia on 4/20/2015.
 */
@Controller
public class RootController {

    @RequestMapping("/")
    public String root(){ return "Index"; }

    @RequestMapping("/csrf_unsecured")
    public String csrfAttackToUnsecuredApp(){ return "CSRF_Unsecured"; }

    @RequestMapping("/csrf_secured")
    public String csrfAttackToSecuredApp(){ return "CSRF_Secured"; }

    @RequestMapping("/clickjacking_unsecured")
    public String clickJackingAttackToUnsecuredApp(){ return "ClickJacking_Unsecured"; }

    @RequestMapping("/clickjacking_secured")
    public String clickJackingAttackToSecuredApp(){ return "ClickJacking_Secured"; }

}