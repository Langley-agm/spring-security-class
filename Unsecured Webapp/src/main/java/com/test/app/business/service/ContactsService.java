package com.test.app.business.service;

import com.test.app.data.dao.ContactsDAO;
import com.test.app.data.dao.ContactsDAOMock;
import com.test.app.data.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ContactsService {

    private ContactsDAO contactsDAO;

    @Autowired
    public ContactsService(ContactsDAO contactsDAO){
        this.contactsDAO = contactsDAO;
    }

    public Collection<Contact> getContacts(){
        return contactsDAO.getAll();
    }

    public void deleteContact(Integer id){
        contactsDAO.delete(id);
    }

}
