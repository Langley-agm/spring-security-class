package com.test.app.presentation.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

@Controller
public class ContactsWebController {

    @RequestMapping("/contacts")
    public String getContacts(){
        return "Contacts";
    }

}