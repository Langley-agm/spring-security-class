package com.test.app.presentation.rest.controller;

import com.test.app.business.service.ContactsService;
import com.test.app.data.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

@RestController
@RequestMapping("/rest")
public class ContactsRestController {

    private ContactsService contactsService;

    @Autowired
    public ContactsRestController(ContactsService contactsService){
        this.contactsService = contactsService;
    }

    @RequestMapping("/contacts")
    public Collection<Contact> contacts() {
        return contactsService.getContacts();
    }

    @RequestMapping(value = "/contact", method = DELETE, params = "id")
    public void contacts(Integer id) {
        contactsService.deleteContact(id);
    }

}
