package com.test.app.data.dao;

import com.test.app.data.entities.Contact;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ContactsDAOMock implements ContactsDAO{

    private static final Map<Integer, Contact> contacts = new HashMap<Integer, Contact>();

    static {
        create( new Contact("Armando","Garcia","111-111-1111") );
        create( new Contact("Karla","Martin","222-222-2222") );
        create( new Contact("Alan","Diaz","333-333-3333") );
        create( new Contact("Luis","Romo","333-333-3333") );
        create( new Contact("Hector","Chavez","333-333-3333") );
    }

    public Collection<Contact> getAll(){ return contacts.values(); }

    public void delete(Integer id){
        contacts.remove(id);
    }

    private static void create(Contact contact){
        contact.setId( contacts.size() + 1 );
        contacts.put(contact.getId(), contact);
    }

}
