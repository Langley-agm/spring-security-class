package com.test.app.data.dao;

import com.test.app.data.entities.Contact;

import java.util.Collection;

public interface ContactsDAO {
    Collection<Contact> getAll();
    void delete(Integer id);
}
